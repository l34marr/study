Python 3.9 技術手冊
陳信良

https://github.com/ihongChen/py-tech

```
>>> class Point:
...     def __init__(self, x, y):
...         self.x = x
...         self.y = y
...     def __repr__(self):
...         return 'Point({}, {})'.format(self.x, self.y)
... 
>>> p1 = Point(1, 1)
>>> p2 = Point(2, 2)
>>> p3 = Point(1, 1)
>>> ps = {p1, p2, p3}
>>> ps
{Point(1, 1), Point(2, 2), Point(1, 1)}
>>> 
```
```
    def __eq__(self, that):
        if hasattr(that, 'x') and hasattr(that, 'y'):
            return self.x == that.x and self.y == that.y
        return False

    def __hash__(self):
        return 41 * (41 + self.x) + self.y
```
