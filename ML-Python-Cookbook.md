Machine Learning with Python Cookbook
- Practical Solutions from Preprocessing to Deep Learning
by Chris Albon

https://www.oreilly.com/library/view/machine-learning-with/9781491989371/

```
import pandas as pd

url = 'https://tinyurl.com/titanic-csv'
dataframe = pd.read_csv(url)
dataframe.head(5)
```
```
dataframe.iloc[:4]
```
```
dataframe = dataframe.set_index(dataframe['Name])
dataframe.loc['Allen, Miss Elisabeth Walton']
```
```
dataframe[(dataframe['Sex'] == 'female') & (dataframe['Age'] >= 65)]
```
```
dataframe['Sex'].replace('female', 'Woman')
dataframe.replace(r'1st', 'First', regex=True)
```
```
dataframe.rename(columns={'PClass': 'Passenger Class', 'Sex': 'Gender'})
```
```
import collections

column_names = collections.defaultdict(str)
for name in dataframe.cloumns:
    column_names[name]

column_names
```
